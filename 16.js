


let f0 = +prompt('Enter the Fibonacci number 0', 0);
while (isNaN(f0) || Math.trunc(f0) < f0 ) {
    f0 = +prompt('There was a mistake. Re-enter the number F0!', f0);
}
let f1 = +prompt('Enter the Fibonacci number 1', 0);
while (isNaN(f1) || Math.trunc(f1) < f1) {
    f1 = +prompt('There was a mistake. Re-enter the number F1!', f1);
}
let n = +prompt('Enter the n - number of Gen. Fibonacci number', 0);
while (isNaN(n) || Math.trunc(n) < n )   {
    n = +prompt('There was a mistake. Re-enter the number!', n);
}
console.log(f0);
console.log(f1);
console.log(n); 
if (n === 0) {
    alert(`The F 0 number  in Generalized Fibonacci sequence with F0 = ${f0} and F1 = ${f1}    IS   ${f0}`)
} else if (n === 1) {
    alert(`The F 1 number  in Generalized Fibonacci sequence with F0 = ${f0} and F1 = ${f1}    IS   ${f1}`)
} else if (n > 1) {
    alert(`The F (${n}) number  in Generalized Fibonacci sequence with F0 = ${f0} and F1 = ${f1}    IS   ${operateP(f0, f1, n)}`)
} else if (n < 0) {
    alert(`The F (${n}) number  in Generalized Fibonacci sequence with F0 = ${f0} and F1 = ${f1}    IS   ${operateN(f0, f1, n)}`);
}

function operateP(f0, f1, n) {
    if (n === 0) {
        return f0;
    }
    else if (n === 1) {
        return f1;
    }
    else if (n === 2) {
        return f0 + f1;
    } else  {
        return operateP(f0, f1, n - 2) + operateP(f0, f1, n - 1);
    } 
}

function operateN(f0, f1, n) {
    if (n === 0) {
        return f0;
    }
    else if (n === -1) {
        return f1 - f0;
    }
    else {
        return operateN(f0, f1, n + 2) - operateN(f0, f1, n + 1);
    }    
}
